#!/bin/bash

source color.sh

CARD_DIR="$HOME/cards"
NEW_CARDS_PER_DAY="20"


DECKS="$(find -maxdepth 1 -type d | tail +2)"
DATE_S="$(date +%s)"
DATE_D="$((DATE_S/86400))"

NONEW=""
NOMOVE=""
BOXES=""

function load_box() {
    TIME_INTERVAL="$(basename "$1")"
    CARDS="$(find "$1" -type f)"

    if [ -n "$CARDS" ] ; then
        while read -r card; do
            LAST_DATE_D="$(sed -e '3!d' "$card")"
            LAST_TIME_D="$((DATE_D - LAST_DATE_D))"

            if [ "$LAST_TIME_D" -ge "$TIME_INTERVAL" ] ; then
                printf "$card\n"
            fi
        done <<< "$CARDS"
    fi
}

function run_card() {
    FRONT="$(sed -e '1!d' "$1")"
    BACK="$(sed -e '2!d' "$1")"

    set_fg "$COLOR_YELLOW"
    printf "$FRONT"
    read < /dev/tty
    set_fg "$COLOR_GREEN"
    printf "$BACK\n"
    reset_fg
    printf "Got it? "
    set_bold
    printf "[Y/n] "
    reset_bold
    read CHOICE < /dev/tty

    sed -i "3s/.*$/$DATE_D/" "$1"

    if [ "$CHOICE" = "n" ] ; then
        return 1
    else
        return 0
    fi
}

function move_card() {
    CARD="$(basename "$1")"
    BOX="$(basename "$(dirname "$1")")"
    DECK="$(dirname "$(dirname "$1")")"
    
    if [ -z "$BOXES" ] ; then
        printf "Error: can't move card, no boxes loaded\n" > /dev/tty
        return 1
    fi

    NEXT_BOX="9999999"

    for i in $BOXES ; do
        BOX_NB="$(basename "$i")"
        if [ "$BOX_NB" -lt "$NEXT_BOX" -a "$BOX_NB" -gt "$BOX" ] ; then
            NEXT_BOX="$BOX_NB"
        fi
    done

    case "$2" in
        up)
            if [ "$NEXT_BOX" -lt 9999999 ] ; then
                BOX="$NEXT_BOX"
                mv "$1" "$DECK/$BOX/"
            fi
            ;;
        down)
            if [ "$BOX" != 0 ] ; then
                BOX="0"
                mv "$1" "$DECK/$BOX/"
            fi
            ;;
    esac
    printf "$DECK/$BOX/$CARD"
}

function add_card() {
    DIR="$CARD_DIR/$1/0"
    CARDNAME="$(date +%y%m%d%H%M%S)"

    printf "%s\n%s\nnever\n" "$2" "$3" > "$DIR/$CARDNAME"
}

function load_deck() {
    DIR="$CARD_DIR/$1"
    STACK=""

    if [ ! -d "$DIR" ] ; then
        printf "Error: deck doesn't exist\n"
        exit 1
    fi

    while read -r box; do
        TMP="$(load_box "$box")"
        if [ "$(basename "$box")" -eq "0" ] ; then
            TMP="$(echo "$TMP" | head -n "$NEW_CARDS_PER_DAY")"
        fi
        if [ -n "$TMP" ] ; then
            echo "Loading $(echo "$TMP" | wc -l) from box $box" > /dev/tty
            STACK="$STACK$TMP\n"
        fi
    done <<< "$BOXES"

    printf "$STACK"
}

case "$1" in
    train)
        if [ -d "$CARD_DIR/$2" ] ; then
            BOXES="$(find "$CARD_DIR/$2" -type d -regex "$CARD_DIR/$2/[0-9]*")"
            STACK="$(load_deck "$2")"
            if [ -n "$STACK" ] ; then
                LEFT="$(wc -l <<< "$STACK")"
                AGAIN="0"

                while [ -n "$STACK" ] ; do
                    read card <<< "$STACK"
                    set_bold
                        printf "["
                        set_fg "$COLOR_BLUE" ; printf "$LEFT" ; reset_fg
                        printf "|"
                        set_fg "$COLOR_RED" ; printf "$AGAIN" ; reset_fg
                        printf "]\n"
                    reset_bold
                    if ! run_card "$card" ; then
                        card="$(move_card "$card" "down")"
                        STACK="$STACK\n$card\n"
                        AGAIN="$((AGAIN+1))"
                    else
                        move_card "$card" "up" > /dev/null
                    fi

                    STACK="$(printf "$STACK" | tail +2)"
                    LEFT="$((LEFT-1))"

                    if [ "$LEFT" = "0" ] ; then
                        LEFT="$AGAIN"
                        AGAIN="0"
                    fi
                done
            else
                printf "Nothing to learn today\n"
                exit 0
            fi
        else
            printf "Error: can't find deck $2\n"
            exit 1
        fi
        ;;
    new)
        if ! [ -d "$CARD_DIR" ] ; then
            printf "$CARD_DIR doesn't exist, should I create it ? [Y/n]"
            read CHOICE
            if [ "$CHOICE" = "n" ] ; then
                printf "Error: can't continue without a valid CARD_DIR, please set it up.\n"
                exit 1
            fi
        fi
        mkdir -p "$CARD_DIR/$2"
        for i in $(seq 0 5) ; do
            mkdir "$CARD_DIR/$2/$i"
        done
        ;;
    add)
        if [ -d "$CARD_DIR/$2" ] ; then
            printf "Adding new cards in deck $CARD_DIR/$2\n"
            while true ; do
                printf "Front:\n"
                read FRONT
                printf "Back:\n"
                read BACK

                if [ -n "$FRONT" -a -n "$BACK" ] ; then
                    add_card "$2" "$FRONT" "$BACK"
                else
                    exit 0
                fi
            done
        else
            printf "Error: can't find deck $2\n"
        fi
        ;;
    search)
        if [ -d "$CARD_DIR/$2" ] ; then
            if [ -n "$3" ] ; then
                RES="$(grep -r "$3" "$CARD_DIR/$2" --exclude-dir=".git" | cut -d: -f1)"
                for i in $RES ; do
                    set_bold
                    printf "$i:\n"
                    reset_bold
                    cat "$i"
                done
            fi
        fi
        ;;
    *)
        printf "Usage: $0 <command>\n"
        printf "Commands:\n"
        printf "    train <deck>: train on the given card deck\n"
        printf "    new <deck>: create a new deck with the specified name\n"
        printf "    add <deck>: add cards to the new deck\n"
        printf "    search <deck> <expr>: displays all cards matching <expr>\n"
        ;;
esac
